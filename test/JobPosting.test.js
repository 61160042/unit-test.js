const assert = require('assert')
const { checkEnableTime } = require("../JobPosting")

describe('JobPosting', function () {
  it('return true: เวลาอยู่ระหว่างเวลาเริ่มต้นและเวลาสิ้นสุด', function () {
    //Arrage
    const startDate = new Date(2021, 0, 31)
    const endDate = new Date(2021, 1, 5)
    const today = new Date(2021, 1, 2)
    const expectedResult = true
    //Act
    const actualResult = checkEnableTime(startDate, endDate, today)

    //Assert
    assert.strictEqual(actualResult, expectedResult)
  }),
    it('return true: เวลาเท่ากับเวลาเริ่มต้น', function () {
      //Arrage
      const startDate = new Date(2021, 0, 31)
      const endDate = new Date(2021, 1, 5)
      const today = new Date(2021, 0, 31)
      const expectedResult = true
      //Act
      const actualResult = checkEnableTime(startDate, endDate, today)

      //Assert
      assert.strictEqual(actualResult, expectedResult)
    }),
    it('return true: เวลาเท่ากับเวลาสิ้นสุด', function () {
      //Arrage
      const startDate = new Date(2021, 0, 31)
      const endDate = new Date(2021, 1, 5)
      const today = new Date(2021, 1, 5)
      const expectedResult = true
      //Act
      const actualResult = checkEnableTime(startDate, endDate, today)

      //Assert
      assert.strictEqual(actualResult, expectedResult)
    }),
    it('return false: เวลาน้อยกว่าเวลาเริ่มต้น', function () {
      //Arrage
      const startDate = new Date(2021, 0, 31)
      const endDate = new Date(2021, 1, 5)
      const today = new Date(2021, 0, 30)
      const expectedResult = false
      //Act
      const actualResult = checkEnableTime(startDate, endDate, today)

      //Assert
      assert.strictEqual(actualResult, expectedResult)
    }),
    it('return false: เวลามากกว่าเวลาสิ้นสุด', function () {
      //Arrage
      const startDate = new Date(2021, 0, 31)
      const endDate = new Date(2021, 1, 5)
      const today = new Date(2021, 1, 6)
      const expectedResult = false
      //Act
      const actualResult = checkEnableTime(startDate, endDate, today)

      //Assert
      assert.strictEqual(actualResult, expectedResult)
    })
})

