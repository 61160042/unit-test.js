exports.checkEnableTime = function (startDate, endDate, today) {
  if (today < startDate) {
    return false
  } else if (today > endDate) {
    return false
  }
  return true
}